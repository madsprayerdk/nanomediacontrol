﻿using System;

namespace NanoMediaControl.Midi
{
    public class MidiInEventArgs : EventArgs
    {
        public MidiInEventArgs(int id, int value)
        {
            Id = id;
            Value = value;
        }

        public int Id { private set; get; }
        public int Value { private set; get; }
    }
}