﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using NanoMediaControl.Midi;
using IWshRuntimeLibrary;
using File = System.IO.File;

namespace NanoMediaControl
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private NanoKontrol2 _midi;

        public const int KEYEVENTF_EXTENDEDKEY = 1;
        public const int KEYEVENTF_KEYUP = 2;
        public const int VK_MEDIA_NEXT_TRACK = 0xB0;
        public const int VK_MEDIA_PREV_TRACK = 0xB1;
        public const int VK_MEDIA_STOP = 0xB2;
        public const int VK_MEDIA_PLAY_PAUSE = 0xB3;


        [DllImport("user32.dll", SetLastError = true)]
        public static extern void keybd_event(byte virtualKey, byte scanCode, uint flags, IntPtr extraInfo);


        public App()
        {
            var shell = new WshShellClass();
            var shortcutPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\NanoVolume.lnk";

            var trayIcon = new NotifyIcon();

            var menuItemExit = new MenuItem{
                Index = 1,
                Text = @"Exit"
            };

            var menuItemStartup = new MenuItem
            {
                Index = 0,
                Text = @"Start on Boot",
                Checked = File.Exists(shortcutPath)
            };

            menuItemStartup.Click += (sender, args) =>
            {
                if (File.Exists(shortcutPath))
                {
                    File.Delete(shortcutPath);
                    menuItemStartup.Checked = false;
                }
                else
                {
                    var shortcut = (IWshShortcut)shell.CreateShortcut(shortcutPath);
                    shortcut.TargetPath = Assembly.GetEntryAssembly().Location;
                    shortcut.Description = @"Startup for NanoVolume";
                    shortcut.WorkingDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                    shortcut.Save();
                    menuItemStartup.Checked = true;
                }
            };

            menuItemExit.Click += (sender, args) =>
            {
                _midi.Stop();
                trayIcon.Visible = false;
                Environment.Exit(0);
            };

            trayIcon.ContextMenu = new ContextMenu()
            {
                MenuItems =
                {
                    menuItemStartup,
                    menuItemExit
                }
            };

            trayIcon.Text = @"NanoMediaControl";
            trayIcon.Icon = new Icon("app.ico");
            trayIcon.Visible = true;

            _midi = new NanoKontrol2();
            _midi.PlayPressed += _midi_PlayPressed;
            _midi.PausePressed += MidiOnPausePressed;
            _midi.PrevPressed += _midi_PrevPressed;
            _midi.NextPressed += _midi_NextPressed;
            _midi.Start();
        }

        private void _midi_NextPressed(object sender, MidiInEventArgs args)
        {
            if (args.Value > 0)
                keybd_event(VK_MEDIA_NEXT_TRACK, 0, KEYEVENTF_EXTENDEDKEY, IntPtr.Zero);
        }

        private void _midi_PrevPressed(object sender, MidiInEventArgs args)
        {
            if (args.Value > 0)
                keybd_event(VK_MEDIA_PREV_TRACK, 0, KEYEVENTF_EXTENDEDKEY, IntPtr.Zero);
        }

        private void MidiOnPausePressed(object sender, MidiInEventArgs args)
        {
            if (args.Value > 0)
                keybd_event(VK_MEDIA_STOP, 0, KEYEVENTF_EXTENDEDKEY, IntPtr.Zero);
        }

        private void _midi_PlayPressed(object sender, MidiInEventArgs args)
        {
            if (args.Value > 0)
                keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENTF_EXTENDEDKEY, IntPtr.Zero);
        }
    }
}
